import React from 'react';
import {Route, Switch} from 'react-router-dom';

import {
  Container,
  Window,
} from './components';
import Notes from './Notes';



function App() {
  return (
    <Container>
      <Window>
        <Switch>
          <Route path="/">
            <Notes />
          </Route>
        </Switch>
      </Window>
    </Container>
  );
}

export default App;
