import {RouterState} from 'connected-react-router';

import {State as NotesState} from './Notes/types';


export type State = {
  notes: NotesState,
  router: RouterState,
};

