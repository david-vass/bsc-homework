import {fork} from 'redux-saga/effects';

import notesSagas from './Notes/sagas';


function* sagas() {
  yield fork(notesSagas);
}


export default sagas;

