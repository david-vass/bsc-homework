import styled from 'styled-components';


export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
  background: #c3c3ff;
`;

export const Window = styled.div`
  width: 500px;
  height: 400px;
  border: 1px solid blue;
  background: #fff;
  box-shadow: 10px 10px 0 0 blue;
`;

