import styled from 'styled-components';


type LinkProps = {
  disabled?: boolean,
};

export const Link = styled.span<LinkProps>`
  display: inline-block;
  color: blue;
  cursor: ${({disabled}) => disabled ? 'default' : 'pointer'};

  &:hover, &:focus {
    text-decoration: ${({disabled}) => disabled ? 'none' : 'underline'};
  }

  opacity: ${({disabled}) => disabled ? 0.5 : 1}
`;

export const CenteredContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const LoadingMessage = CenteredContent;

export const ErrorMessage = CenteredContent;

export const TryAgainButton = styled(Link.withComponent('button'))`
  padding-top: 1em;
`;

export const Container = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  flex-direction: column;
`;

export const Content = styled.div`
  padding: 10px 20px 20px;
  position: relative;
  overflow: auto;
  flex-grow: 1;
  cursor: text;
`;

export const Title = styled.h3`
  font-weight: bold;
  margin-bottom: 1em;
`;

export const Body = styled.div`

`;

export const Editor = styled(Content.withComponent('textarea'))`
  font-family: Lato;
  width: 100%;
  border: 0;
  outline: none;
  resize: none;
  flex-grow: 1;
`;

export const Toolbar = styled.div`
  padding: 20px 0 10px;
  margin: 0 20px;
  border-bottom: 1px solid blue;
  display: flex;
  justify-content: space-between;
`;

export const ToolbarItem = Link;

export const ToolbarStatusMessage = styled.div`
  color: #666;
`;

