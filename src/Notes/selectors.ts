import {State} from '../types';
import {
  State as NotesState,
  Entity,
  EntityState,
  NewState,
} from './types';


function selectNotesState(state: State): NotesState {
  return state.notes;
}

export function selectNotes(
  state: State,
): EntityState[] {
  const {notes, entities} = selectNotesState(state);
  return notes.map((id) => entities[id]);
}

export function selectIsLoading(
  state: State,
): NotesState['loading'] {
  return selectNotesState(state).loading;
}

export function selectError(
  state: State,
): NotesState['error'] {
  return selectNotesState(state).error;
}


export function makeSelectNote(id: string) {
  return function(state: State): Entity | undefined {
    const note = selectNotesState(state).entities[id];
    if (!note) {return undefined;}
    const {title, body} = note;
    return {id: note.id, title, body};
  }
}

export function makeSelectNoteIsLoading(id: string) {
  return function(state: State): EntityState['loading'] {
    const note = selectNotesState(state).entities[id];
    if (!note) {return true;}
    return note.loading;
  }
}

export function makeSelectNoteError(id: string) {
  return function(state: State): EntityState['error'] | null {
    const note = selectNotesState(state).entities[id];
    if (!note) {return null;}
    return note.error;
  }
}

export function makeSelectNoteIsSaving(id: string) {
  return function(state: State): EntityState['saving'] {
    const note = selectNotesState(state).entities[id];
    if (!note) {return false;}
    return note.saving;
  }
}


function selectNewState(state: State): NewState {
  return selectNotesState(state).new;
}

export function selectNewIsLoading(
  state: State,
): NewState['loading'] {
  return selectNewState(state).loading;
}

export function selectNewError(
  state: State,
): NewState['error'] {
  return selectNewState(state).error;
}

