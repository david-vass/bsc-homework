import React from 'react';
import {
  Route,
  Switch,
  useRouteMatch,
} from 'react-router-dom';

import List from './List';
import New from './New';
import View from './View';
import Edit from './Edit';


function Notes() {
  const {path} = useRouteMatch();

  return (
    <Switch>
      <Route exact path={`${path}`}>
        <List />
      </Route>
      <Route exact path={`${path}new`}>
        <New />
      </Route>
      <Route exact path={`${path}:id`}>
        <View />
      </Route>
      <Route exact path={`${path}:id/edit`}>
        <Edit />
      </Route>
    </Switch>
  );
}


export default Notes;

