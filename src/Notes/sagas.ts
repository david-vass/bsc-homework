import {
  takeLatest,
  put,
  call,
} from 'redux-saga/effects';
import {SagaIterator} from 'redux-saga';
import {push} from 'connected-react-router';

import api from '../api';

import {ACTIONS} from './enums';
import {
  fetchNotesSuccess,
  fetchNotesFail,
  fetchNoteSuccess,
  fetchNoteFail,
  saveNoteSuccess,
  saveNoteFail,
  createNoteSuccess,
  createNoteFail,
  deleteNoteSuccess,
} from './actions';
import {
  FetchNoteAction,
  SaveNoteAction,
  CreateNoteAction,
  DeleteNoteAction,
} from './types';


function* fetchNotes(): SagaIterator {
  try {
    const response = yield call(api.get, '/notes');

    yield put(fetchNotesSuccess({data: response.data}));
  } catch (error) {
    yield put(fetchNotesFail());
  }
}

function* fetchNote(action: FetchNoteAction): SagaIterator {
  const {id} = action.payload;
  try {
    const response = yield call(api.get, `/notes/${id}`);
    const data = {
      ...response.data,
      id,
    };

    yield put(fetchNoteSuccess({id, data}));
  } catch (e) {
    yield put(fetchNoteFail({id}));
  }
}

function* saveNote(action: SaveNoteAction): SagaIterator {
  const {id, data} = action.payload;
  try {
    const {title, body} = data;
    yield call(api.put,
      `/notes/${id}`,
      {
        id,
        title,
        body,
      },
    );

    yield put(saveNoteSuccess({id}));
  } catch (e) {
    yield put(saveNoteFail({id}));
  }
}

function* createNote(action: CreateNoteAction): SagaIterator {
  try {
    const response = yield call(api.post,
      '/notes',
    );
    const {id} = response.data;
    yield put(createNoteSuccess());
    yield put(push(`/${id}/edit`));
  } catch (error) {
    yield put(createNoteFail());
  }
}

function* deleteNote(action: DeleteNoteAction): SagaIterator {
  const {id} = action.payload;
  yield call(api.delete, `/notes/${id}`);

  yield put(deleteNoteSuccess({id}));
}

function* sagas() {
  yield takeLatest(ACTIONS.FETCH_NOTES, fetchNotes);
  yield takeLatest(ACTIONS.FETCH_NOTE, fetchNote);
  yield takeLatest(ACTIONS.SAVE_NOTE, saveNote);
  yield takeLatest(ACTIONS.CREATE_NOTE, createNote);
  yield takeLatest(ACTIONS.DELETE_NOTE, deleteNote);
}


export default sagas;

