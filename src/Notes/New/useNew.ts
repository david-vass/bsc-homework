import {
  useEffect,
} from 'react';
import {
  useDispatch,
  useSelector,
} from 'react-redux';

import {
  createNote,
} from '../actions';
import {
  selectNewIsLoading,
  selectNewError,
} from '../selectors';


function useNew() {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectNewIsLoading);
  const error = useSelector(selectNewError);

  useEffect(() => {
    dispatch(createNote());
  }, [dispatch]);

  const handleTryAgainClick = () => {
    dispatch(createNote());
  }

  return {
    isLoading,
    error,
    handleTryAgainClick,
  };
}


export default useNew;
