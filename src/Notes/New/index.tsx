import React from 'react';

import {
  LoadingMessage,
  ErrorMessage,
  TryAgainButton,
} from '../components';
import useNew from './useNew';


function New() {
  const {
    isLoading,
    error,
    handleTryAgainClick,
  } = useNew();

  if (!isLoading && error) {
    return (
      <ErrorMessage>
        Failed to create new note.
        <TryAgainButton onClick={handleTryAgainClick}>
          [Try again]
        </TryAgainButton>
      </ErrorMessage>
    );
  }

  return (
    <LoadingMessage>
      Creating note...
    </LoadingMessage>
  );
}


export default New;

