import React from 'react';
import {
  Link as RouterLink,
} from 'react-router-dom';

import {
  Container,
  LoadingMessage,
  ErrorMessage,
  TryAgainButton,
  Editor,
  Toolbar,
  ToolbarItem,
  ToolbarStatusMessage,
} from '../components';

import useEdit from './useEdit';


function Edit() {
  const {
    isLoading,
    error,
    isSaving,
    handleTryAgainClick,
    id,
    wasSaving,
    value,
    handleValueChange,
    editorRef,
  } = useEdit();

  if (isLoading || (error && isSaving)) {
    return <LoadingMessage>Loading...</LoadingMessage>;
  }

  if (error) {
    return (
      <ErrorMessage>
        Failed to save the note!
        <TryAgainButton onClick={handleTryAgainClick}>
          [Try again]
        </TryAgainButton>
      </ErrorMessage>
    );
  }

  return (
    <Container>
      <Toolbar>
        {!isSaving
          ? (
            <ToolbarItem
              as={RouterLink}
              to={`/${id}`}
            >
              [Done]
            </ToolbarItem>
          ) : (
            <ToolbarItem
              disabled
            >
              [Done]
            </ToolbarItem>
          )}
        
        {wasSaving && (
          <ToolbarStatusMessage>
            {isSaving ? 'Saving...' : 'Saved!'}
          </ToolbarStatusMessage>
        )}
      </Toolbar>
      <Editor
        value={value}
        onChange={handleValueChange}
        ref={editorRef}
        placeholder="Start typing..."
        autoFocus
      />
    </Container>
  );
}


export default Edit;

