import React, {
  useMemo,
  useState,
  useRef,
  useEffect,
} from 'react';
import {
  useDispatch,
  useSelector,
} from 'react-redux';
import {
  useParams,
} from 'react-router-dom';

import {
  fetchNote,
  saveNote,
} from '../actions';
import {
  makeSelectNote,
  makeSelectNoteIsLoading,
  makeSelectNoteError,
  makeSelectNoteIsSaving,
} from '../selectors';
import {
  Entity,
} from '../types';


const getDefaultValue = (note?: Entity): string => {
  if (!note) {return '';}

  let value = '';
  if (note.title) {
    value = note.title;
  }
  if (note.body) {
    value = value.length ? `${value}\n\n${note.body}` : note.body;
  }

  return value;
};

function useEdit() {
  const {id} = useParams<{id: string}>();
  const dispatch = useDispatch();
  const selectNote = useMemo(() => makeSelectNote(id), [id]);
  const selectIsLoading = useMemo(() => makeSelectNoteIsLoading(id), [id]);
  const selectError = useMemo(() => makeSelectNoteError(id), [id]);
  const note = useSelector(selectNote);
  const isLoading = useSelector(selectIsLoading);
  const error = useSelector(selectError);

  const selectIsSaving = useMemo(() => makeSelectNoteIsSaving(id), [id]);
  const isSaving = useSelector(selectIsSaving);
  const [value, setValue] = useState(getDefaultValue(note));
  const [wasSaving, setWasSaving] = useState(false);
  const editorRef = useRef(null);
  const timerRef = useRef<ReturnType<typeof setTimeout> | null>(null);

  useEffect(() => {
    dispatch(fetchNote({id}));
  }, [dispatch, id]);

  useEffect(() => {
    setValue(getDefaultValue(note));
  }, [isLoading]);

  const handleSaveChanges = (value: string) => {
    if (!note) {return;}

    const [title, ...bodyParts] = value.split('\n');
    const body = bodyParts.join('\n').trim();
    dispatch(saveNote({
      id: note.id,
      data: {
        title,
        body,
      },
    }));
    setWasSaving(true);
  };

  const handleValueChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const {value} = e.currentTarget;
    setValue(value);
    if (timerRef.current) {clearTimeout(timerRef.current);}

    timerRef.current = setTimeout(() => {
      handleSaveChanges(value);
    }, 500);
  };

  const handleTryAgainClick = () => {
    handleSaveChanges(value);
  };

  return {
    isLoading,
    error,
    isSaving,
    handleTryAgainClick,
    id,
    wasSaving,
    value,
    handleValueChange,
    editorRef,
  };
}


export default useEdit;
