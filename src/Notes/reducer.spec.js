import reducer, {
  defaultEntity,
  defaultState,
} from './reducer';
import {
  fetchNotes,
  fetchNotesSuccess,
  fetchNotesFail,

  fetchNote,
  fetchNoteSuccess,
  fetchNoteFail,

  createNote,
  createNoteSuccess,
  createNoteFail,

  saveNote,
  saveNoteSuccess,
  saveNoteFail,

  deleteNote,
  deleteNoteSuccess,
} from './actions';
import {
  ERROR_TYPES,
} from './enums';


describe('reducer', () => {
  it('should return an empty state', () => {
    expect(reducer(undefined, {}))
      .toEqual(defaultState);
  });

  describe('Fetch notes', () => {
    it('get', () => {
      expect(reducer(undefined, fetchNotes()))
        .toEqual({
          ...defaultState,
          loading: true,
        });
    });

    it('success', () => {
      expect(reducer(
        {
          ...defaultState,
          loading: true,
        },
        fetchNotesSuccess({
          data: [
            {id: '1', title: 'Title 1'},
            {id: '2', title: 'Title 2'},
            {id: '3', title: 'Title 3'},
            {id: '4', title: 'Title 4'},
          ],
        })),
      )
        .toEqual({
          ...defaultState,
          loading: false,
          entities: {
            '1': {
              ...defaultEntity,
              id: '1',
              title: 'Title 1',
            },
            '2': {
              ...defaultEntity,
              id: '2',
              title: 'Title 2',
            },
            '3': {
              ...defaultEntity,
              id: '3',
              title: 'Title 3',
            },
            '4': {
              ...defaultEntity,
              id: '4',
              title: 'Title 4',
            },
          },
          notes: ['1', '2', '3', '4'],
        });
    });

    it('fail', () => {
      expect(reducer({
        ...defaultState,
        loading: true,
      }, fetchNotesFail()))
        .toEqual({
          ...defaultState,
          loading: false,
          error: ERROR_TYPES.FETCH_NOTES_ERROR,
        });
    });
  });

  describe('Fetch note', () => {
    it('get', () => {
      expect(reducer(undefined, fetchNote({id: '1'})))
        .toEqual({
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              loading: true,
              id: '1',
            },
          },
        });
    });

    it('success', () => {
      expect(reducer(
        {
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              loading: true,
            },
          },
        },
        fetchNoteSuccess({
          id: '1',
          data: {id: '1', title: 'Title 1'},
        })),
      )
        .toEqual({
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              title: 'Title 1',
              id: '1',
              loading: false,
            },
          },
        });
    });

    it('fail', () => {
      expect(reducer(
        {
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              loading: true,
            },
          },
        },
        fetchNoteFail({id: '1'}),
      ))
        .toEqual({
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              loading: false,
              error: ERROR_TYPES.FETCH_NOTE_ERROR,
            },
          },
        });
    });
  });

  describe('Create note', () => {
    it('post', () => {
      expect(reducer(undefined, createNote()))
        .toEqual({
          ...defaultState,
          new: {
            loading: true,
            error: null,
          },
        });
    });

    it('success', () => {
      expect(reducer(
        {
          ...defaultState,
          new: {
            loading: true,
            error: null,
          },
        },
        createNoteSuccess({id: '1'})),
      )
        .toEqual({
          ...defaultState,
          new: {
            loading: false,
            error: null,
          },
        });
    });

    it('fail', () => {
      expect(reducer(
        {
          ...defaultState,
          new: {
            loading: true,
            error: null,
          },
        },
        createNoteFail(),
      ))
        .toEqual({
          ...defaultState,
          new: {
            loading: false,
            error: ERROR_TYPES.CREATE_NOTE_ERROR,
          },
        });
    });
  });

  describe('Save note', () => {
    it('put', () => {
      expect(reducer(
        undefined,
        saveNote({
          id: '1',
          data: {title: 'Title1', body: 'Body'},
        }),
      ))
        .toEqual({
          ...defaultState,
          entities: {
            '1': {
              saving: true,
              title: 'Title1',
              body: 'Body',
            },
          },
        });
    });

    it('success', () => {
      expect(reducer(
        {
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              id: '1',
              saving: true,
            },
          },
        },
        saveNoteSuccess({id: '1'}),
      ))
        .toEqual({
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              saving: false,
              id: '1',
            },
          },
        });
    });

    it('fail', () => {
      expect(reducer(
        {
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              saving: true,
              id: '1',
            },
          },
        },
        saveNoteFail({id: '1'}),
      ))
        .toEqual({
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              saving: false,
              id: '1',
              error: ERROR_TYPES.SAVE_NOTE_ERROR,
            },
          },
        });
    });
  });

  describe('Delete note', () => {
    it('delete', () => {
      expect(reducer(
        undefined,
        deleteNote({id: '1'}),
      ))
        .toEqual({
          ...defaultState,
          entities: {
            '1': {
              deleting: true,
            },
          },
        });
    });

    it('success', () => {
      expect(reducer(
        {
          ...defaultState,
          entities: {
            '1': {
              ...defaultEntity,
              id: '1',
              deleting: true,
            },
          },
        },
        deleteNoteSuccess({id: '1'}),
      ))
        .toEqual({
          ...defaultState,
          entities: {},
        });
    });
  });
});

