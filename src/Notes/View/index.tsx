import React from 'react';

import {
  Link as RouterLink,
} from 'react-router-dom';
import reactStringReplace from 'react-string-replace';

import {
  LoadingMessage,
  ErrorMessage,
  TryAgainButton,
  Container,
  Content,
  Title,
  Body,
  Toolbar,
  ToolbarItem,
} from '../components';
import useView from './useView';


function View() {
  const {
    isLoading,
    error,
    note,
    handleTryAgainClick,
    handleStartEditing,
    id,
  } = useView();

  if (isLoading) {
    return <LoadingMessage>Loading...</LoadingMessage>;
  }

  if (error || !note) {
    return (
      <ErrorMessage>
        Failed to fetch the note!
        <TryAgainButton onClick={handleTryAgainClick}>
          [Try again]
        </TryAgainButton>
      </ErrorMessage>
    );
  }

  return (
    <Container>
      <Toolbar>
        <ToolbarItem as={RouterLink} to="/">
          [Menu]
        </ToolbarItem>
        <ToolbarItem as={RouterLink} to={`/${id}/edit`}>
          [Edit]
        </ToolbarItem>
      </Toolbar>
      <Content
        onClick={handleStartEditing}
      >
        <Title>{note.title || 'Untitled note'}</Title>
        <Body>
          {reactStringReplace(note.body, '\n', () => <br />)}
        </Body>
      </Content>
    </Container>
  );
}


export default View;
