import {
  useEffect,
  useMemo,
} from 'react';
import {
  useDispatch,
  useSelector,
} from 'react-redux';
import {useParams} from 'react-router-dom';
import {push} from 'connected-react-router';

import {
  fetchNote,
} from '../actions';
import {
  makeSelectNote,
  makeSelectNoteIsLoading,
  makeSelectNoteError,
} from '../selectors';


function useView() {
  const {id} = useParams<{id: string}>();
  const dispatch = useDispatch();
  const selectNote = useMemo(() => makeSelectNote(id), [id]);
  const selectIsLoading = useMemo(() => makeSelectNoteIsLoading(id), [id]);
  const selectError = useMemo(() => makeSelectNoteError(id), [id]);
  const note = useSelector(selectNote);
  const isLoading = useSelector(selectIsLoading);
  const error = useSelector(selectError);

  useEffect(() => {
    dispatch(fetchNote({id}));
  }, [dispatch, id]);

  const handleStartEditing = () => {
    dispatch(push(`/${id}/edit`));
  };

  const handleTryAgainClick = () => {
    dispatch(fetchNote({id}));
  };

  return {
    isLoading,
    error,
    note,
    handleTryAgainClick,
    handleStartEditing,
    id,
  };
}


export default useView;
