import {
  ACTIONS,
  ERROR_TYPES,
} from './enums';

import {
  State,
  EntityState,
  Actions,
} from './types';


export const defaultState: State = {
  new: {
    loading: false,
    error: null,
  },
  entities: {},
  notes: [],
  loading: true,
  error: null,
};

export const defaultEntity: EntityState = {
  loading: false,
  saving: false,
  deleting: false,
  error: null,
  title: '',
  body: '',
  id: '',
}

function notesReducer(
  state: State = defaultState,
  action: Actions,
): State {
  switch(action.type) {
    case ACTIONS.FETCH_NOTES: {
      return {
        ...state,
        loading: true,
      };
    }
    case ACTIONS.FETCH_NOTES_SUCCESS: {
      const {data} = action.payload;
      const notes: string[] = [];
      const entities: {[id: string]: EntityState} = {};
      data.forEach(({id, title}) => {
        notes.push(id);
        entities[id] = {
          ...defaultEntity,
          id,
          title,
        };
      });
      return {
        ...state,
        loading: false,
        entities,
        notes,
        error: null,
      };
    }
    case ACTIONS.FETCH_NOTES_FAIL: {
      return {
        ...state,
        loading: false,
        error: ERROR_TYPES.FETCH_NOTES_ERROR,
      }
    }

    case ACTIONS.FETCH_NOTE: {
      const {id} = action.payload;
      return {
        ...state,
        entities: {
          ...state.entities,
          [id]: {
            ...defaultEntity,
            loading: true,
            id,
          },
        },
      };
    }
    case ACTIONS.FETCH_NOTE_SUCCESS: {
      const {id, data} = action.payload;
      return {
        ...state,
        entities: {
          ...state.entities,
          [id]: {
            ...(state.entities[id] || defaultEntity),
            ...data,
            loading: false,
            id,
          },
        },
      };
    }
    case ACTIONS.FETCH_NOTE_FAIL: {
      const {id} = action.payload;
      return {
        ...state,
        entities: {
          ...state.entities,
          [id]: {
            ...state.entities[id],
            loading: false,
            error: ERROR_TYPES.FETCH_NOTE_ERROR,
          },
        },
      };
    }

    case ACTIONS.CREATE_NOTE: {
      return {
        ...state,
        new: {
          loading: true,
          error: null,
        },
      };
    }
    case ACTIONS.CREATE_NOTE_SUCCESS: {
      return {
        ...state,
        new: {
          loading: false,
          error: null,
        },
      };
    }
    case ACTIONS.CREATE_NOTE_FAIL: {
      return {
        ...state,
        new: {
          loading: false,
          error: ERROR_TYPES.CREATE_NOTE_ERROR,
        },
      };
    }

    case ACTIONS.SAVE_NOTE: {
      const {id, data} = action.payload;
      return {
        ...state,
        entities: {
          ...state.entities,
          [id]: {
            ...state.entities[id],
            ...data,
            saving: true,
          },
        },
      };
    }
    case ACTIONS.SAVE_NOTE_SUCCESS: {
      const {id} = action.payload;
      return {
        ...state,
        entities: {
          ...state.entities,
          [id]: {
            ...state.entities[id],
            saving: false,
            error: null,
          },
        },
      };
    }
    case ACTIONS.SAVE_NOTE_FAIL: {
      const {id} = action.payload;
      return {
        ...state,
        entities: {
          ...state.entities,
          [id]: {
            ...state.entities[id],
            saving: false,
            error: ERROR_TYPES.SAVE_NOTE_ERROR,
          },
        },
      };
    }

    case ACTIONS.DELETE_NOTE: {
      const {id} = action.payload;
      return {
        ...state,       
        entities: {
          ...state.entities,
          [id]: {
            ...state.entities[id],
            deleting: true,
          },
        },
      };
    }
    case ACTIONS.DELETE_NOTE_SUCCESS: {
      const {id} = action.payload;
      const {[id]: entity, ...entities} = state.entities;
      return {
        ...state,
        entities,
        notes: state.notes.filter((noteID) => id !== noteID),
      };
    }

    default: {
      return state;
    }
  }
}


export default notesReducer;

