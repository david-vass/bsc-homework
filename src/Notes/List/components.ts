import styled from 'styled-components';
import {
  Link as RouterLink,
} from 'react-router-dom';
import {
  Link,
  CenteredContent,
} from '../components';


export const Container = styled.ul`
  margin: 20px;
`;

export const MenuItem = styled.li`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid blue;
`;

export const MenuLink = styled(Link.withComponent(RouterLink))`
  padding: 10px 0;
  flex-grow: 1;
`;

export const DeleteButton = styled(Link.withComponent('button'))`
  padding: 10px 0;

  &:disabled {
    opacity: 0.5;
    cursor: default;
  }
`;

export const NoNotesMessage = CenteredContent;

export const CreateNewNote = styled(Link.withComponent(RouterLink))`
  padding-top: 1em;
`;

