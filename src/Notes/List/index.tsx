import React from 'react';

import {
  LoadingMessage,
  ErrorMessage,
  TryAgainButton,
} from '../components';

import {
  Container,
  MenuItem,
  MenuLink,
  DeleteButton,
  NoNotesMessage,
  CreateNewNote,
} from './components';

import useList from './useList';


function List() {
  const {
    isLoading,
    error,
    notes,
    makeHandleDeleteNote,
    handleReloadClick,
  } = useList();

  if (isLoading) {
    return (
      <LoadingMessage>
        Loading...
      </LoadingMessage>
    );
  }

  if (error) {
    return (
      <ErrorMessage>
        Failed to load notes!
        <TryAgainButton onClick={handleReloadClick}>
          [Try again]
        </TryAgainButton>
      </ErrorMessage>
    );
  }

  if (!notes.length) {
    return (
      <NoNotesMessage>
        There are no notes yet.
        <CreateNewNote to="/new">
          [Create new note!]
        </CreateNewNote>
      </NoNotesMessage>
    );
  }

  return (
    <Container>
      <MenuItem>
        <MenuLink to="/new">
          [Create new note]
        </MenuLink>
      </MenuItem>
      {notes.map((item) => (
        <MenuItem key={item.id}>
          {!isLoading && !item.deleting
            ? (
              <MenuLink to={`/${item.id}`}>
                {item.title || 'Untitled note'}
              </MenuLink>
            ) : (
              <MenuLink as="span" disabled>
                {item.title || 'Untitled note'}
              </MenuLink>
            )}
          <DeleteButton
            disabled={item.deleting || isLoading}
            onClick={makeHandleDeleteNote(item.id)}
          >
            [delete]
          </DeleteButton>
        </MenuItem>
      ))}
    </Container>
  );
}


export default List;

