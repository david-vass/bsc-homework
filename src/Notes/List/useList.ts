import {
  useEffect,
} from 'react';
import {
  useDispatch,
  useSelector,
} from 'react-redux';

import {
  fetchNotes,
  deleteNote,
} from '../actions';
import {
  selectNotes,
  selectIsLoading,
  selectError,
} from '../selectors';


function useList() {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsLoading);
  const error = useSelector(selectError);
  const notes = useSelector(selectNotes);

  useEffect(() => {
    dispatch(fetchNotes());
  }, [dispatch]);

  const makeHandleDeleteNote = (id: string) => () => {
    dispatch(deleteNote({id}));
  };

  const handleReloadClick = () => {
    dispatch(fetchNotes());
  };

  return {
    isLoading,
    error,
    notes,
    makeHandleDeleteNote,
    handleReloadClick,
  };
}


export default useList;
