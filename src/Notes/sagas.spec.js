import {call} from 'redux-saga/effects';
import {expectSaga} from 'redux-saga-test-plan';
import {throwError} from 'redux-saga-test-plan/providers';
import {push} from 'connected-react-router';

import api from '../api';

import {
  fetchNotes,
  fetchNotesSuccess,
  fetchNotesFail,

  fetchNote,
  fetchNoteSuccess,
  fetchNoteFail,

  createNote,
  createNoteSuccess,
  createNoteFail,

  saveNote,
  saveNoteSuccess,
  saveNoteFail,

  deleteNote,
  deleteNoteSuccess,
} from './actions';
import sagas from './sagas';


describe('sagas', () => {
  describe('fetchNotes', () => {
    it('success', () => {
      return expectSaga(sagas)
        .provide([
          [call(api.get, '/notes'), {data: 'sample response'}],
        ])
        .dispatch(fetchNotes())
        .put(fetchNotesSuccess({data: 'sample response'}))
        .run();
    });

    it('fail', () => {
      return expectSaga(sagas)
        .provide([
          [call(api.get, '/notes'), throwError(new Error('Some error'))],
        ])
        .dispatch(fetchNotes())
        .put(fetchNotesFail())
        .run();
    });
  });

  describe('createNote', () => {
    it('success', () => {
      return expectSaga(sagas)
        .provide([
          [call(api.post, '/notes'), {data: {id: '123'}}],
        ])
        .dispatch(createNote())
        .put(createNoteSuccess())
        .put(push('/123/edit'))
        .run();
    });

    it('fail', () => {
      return expectSaga(sagas)
        .provide([
          [call(api.post, '/notes'), throwError(new Error('Some error'))],
        ])
        .dispatch(createNote())
        .put(createNoteFail())
        .run();
    });
  });

  describe('fetchNote', () => {
    it('success', () => {
      return expectSaga(sagas)
        .provide([
          [
            call(api.get, '/notes/123'),
            {data: {title: 'Sample title'}},
          ],
        ])
        .dispatch(fetchNote({id: '123'}))
        .put(fetchNoteSuccess({
          id: '123',
          data: {
            title: 'Sample title',
            id: '123',
          },
        }))
        .run();
    });

    it('fail', () => {
      return expectSaga(sagas)
        .provide([
          [call(api.get, '/notes/123'), throwError(new Error('Some error'))],
        ])
        .dispatch(fetchNote({id: '123'}))
        .put(fetchNoteFail({id: '123'}))
        .run();
    });
  });

  describe('saveNote', () => {
    it('success', () => {
      return expectSaga(sagas)
        .provide([
          [
            call(
              api.put,
              '/notes/123',
              {id: '123', title: 'Sample title', body: 'Sample body'},
            ),
            'response',
          ],
        ])
        .dispatch(saveNote({
          id: '123',
          data: {title: 'Sample title', body: 'Sample body'},
        }))
        .put(saveNoteSuccess({id: '123'}))
        .run();
    });

    it('fail', () => {
      return expectSaga(sagas)
        .provide([
          [
            call(
              api.put,
              '/notes/123',
              {id: '123', title: 'Sample title', body: 'Sample body'},
            ),
            throwError(new Error('Some error')),
          ],
        ])
        .dispatch(saveNote({
          id: '123',
          data: {title: 'Sample title', body: 'Sample body'},
        }))
        .put(saveNoteFail({id: '123'}))
        .run();
    });
  });

  describe('deleteNote', () => {
    it('success', () => {
      return expectSaga(sagas)
        .provide([
          [call(api.delete, '/notes/123'), 'response'],
        ])
        .dispatch(deleteNote({id: '123'}))
        .put(deleteNoteSuccess({id: '123'}))
        .run();
    });
  });
});
