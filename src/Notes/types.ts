import {
  ERROR_TYPES,
  ACTIONS,
} from './enums';


export type Entity = {
  id: string,
  title: string,
  body: string,
};


/* STATE ================== */

export type EntityState = Entity & {
  loading: boolean,
  saving: boolean,
  deleting: boolean,
  error: ERROR_TYPES | null,
}

export type NewState = {
  loading: boolean,
  error: ERROR_TYPES | null,
};

export type State = {
  new: NewState,
  entities: {
    [id: string]: EntityState,
  },
  notes: string[],
  loading: boolean,
  error: ERROR_TYPES | null,
};


/* ACTIONS ================== */


/* Fetch notes ================== */

export type FetchNotesAction = {
  type: ACTIONS.FETCH_NOTES,
};

export type FetchNotesSuccessAction = {
  type: ACTIONS.FETCH_NOTES_SUCCESS,
  payload: {
    data: Entity[],
  }
};

export type FetchNotesFailAction = {
  type: ACTIONS.FETCH_NOTES_FAIL,
};

/* Create notes ================== */

export type CreateNoteAction = {
  type: ACTIONS.CREATE_NOTE,
};

export type CreateNoteSuccessAction = {
  type: ACTIONS.CREATE_NOTE_SUCCESS,
};

export type CreateNoteFailAction = {
  type: ACTIONS.CREATE_NOTE_FAIL,
};

/* Fetch note ================== */

export type FetchNoteAction = {
  type: ACTIONS.FETCH_NOTE,
  payload: {
    id: string,
  },
};

export type FetchNoteSuccessAction = {
  type: ACTIONS.FETCH_NOTE_SUCCESS,
  payload: {
    id: string,
    data: Entity,
  }
};

export type FetchNoteFailAction = {
  type: ACTIONS.FETCH_NOTE_FAIL,
  payload: {
    id: string,
  },
};

/* Save note ================== */

export type SaveNoteAction = {
  type: ACTIONS.SAVE_NOTE,
  payload: {
    id: string,
    data: Partial<Entity>
  },
};

export type SaveNoteSuccessAction = {
  type: ACTIONS.SAVE_NOTE_SUCCESS,
  payload: {
    id: string,
  }
};

export type SaveNoteFailAction = {
  type: ACTIONS.SAVE_NOTE_FAIL,
  payload: {
    id: string,
  },
};

/* Delete note ================== */

export type DeleteNoteAction = {
  type: ACTIONS.DELETE_NOTE,
  payload: {
    id: string,
  },
};

export type DeleteNoteSuccessAction = {
  type: ACTIONS.DELETE_NOTE_SUCCESS,
  payload: {
    id: string,
  }
};

export type Actions = FetchNotesAction
  | FetchNotesSuccessAction
  | FetchNotesFailAction

  | CreateNoteAction
  | CreateNoteSuccessAction
  | CreateNoteFailAction

  | FetchNoteAction
  | FetchNoteSuccessAction
  | FetchNoteFailAction

  | SaveNoteAction
  | SaveNoteSuccessAction
  | SaveNoteFailAction

  | DeleteNoteAction
  | DeleteNoteSuccessAction

