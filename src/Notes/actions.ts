import {ACTIONS} from './enums';
import {
  FetchNotesAction,
  FetchNotesSuccessAction,
  FetchNotesFailAction,

  FetchNoteAction,
  FetchNoteSuccessAction,
  FetchNoteFailAction,

  CreateNoteAction,
  CreateNoteSuccessAction,
  CreateNoteFailAction,

  SaveNoteAction,
  SaveNoteSuccessAction,
  SaveNoteFailAction,

  DeleteNoteAction,
  DeleteNoteSuccessAction,
} from './types';



/* Fetch notes ================== */

export function fetchNotes(): FetchNotesAction {
  return {
    type: ACTIONS.FETCH_NOTES,
  };
}

export function fetchNotesSuccess(
  {data}: FetchNotesSuccessAction['payload'],
): FetchNotesSuccessAction {
  return {
    type: ACTIONS.FETCH_NOTES_SUCCESS,
    payload: {data},
  };
}

export function fetchNotesFail(): FetchNotesFailAction {
  return {
    type: ACTIONS.FETCH_NOTES_FAIL,
  };
}

/* Create notes ================== */

export function createNote(): CreateNoteAction {
  return {
    type: ACTIONS.CREATE_NOTE,
  };
}

export function createNoteSuccess(): CreateNoteSuccessAction {
  return {
    type: ACTIONS.CREATE_NOTE_SUCCESS,
  };
}

export function createNoteFail(): CreateNoteFailAction {
  return {
    type: ACTIONS.CREATE_NOTE_FAIL,
  };
}

/* Fetch note ================== */

export function fetchNote(
  {id}: FetchNoteAction['payload'],
): FetchNoteAction {
  return {
    type: ACTIONS.FETCH_NOTE,
    payload: {id},
  };
}

export function fetchNoteSuccess(
  {id, data}: FetchNoteSuccessAction['payload'],
): FetchNoteSuccessAction {
  return {
    type: ACTIONS.FETCH_NOTE_SUCCESS,
    payload: {id, data},
  };
}

export function fetchNoteFail(
  {id}: FetchNoteFailAction['payload'],
): FetchNoteFailAction {
  return {
    type: ACTIONS.FETCH_NOTE_FAIL,
    payload: {id},
  };
}

/* Save note ================== */

export function saveNote(
  {id, data}: SaveNoteAction['payload'],
): SaveNoteAction {
  return {
    type: ACTIONS.SAVE_NOTE,
    payload: {id, data},
  };
}

export function saveNoteSuccess(
  {id}: SaveNoteSuccessAction['payload'],
): SaveNoteSuccessAction {
  return {
    type: ACTIONS.SAVE_NOTE_SUCCESS,
    payload: {id},
  };
}

export function saveNoteFail(
  {id}: SaveNoteFailAction['payload'],
): SaveNoteFailAction {
  return {
    type: ACTIONS.SAVE_NOTE_FAIL,
    payload: {id},
  };
}

/* Delete note ================== */

export function deleteNote(
  {id}: DeleteNoteAction['payload'],
): DeleteNoteAction {
  return {
    type: ACTIONS.DELETE_NOTE,
    payload: {id},
  };
}

export function deleteNoteSuccess(
  {id}: DeleteNoteAction['payload'],
): DeleteNoteSuccessAction {
  return {
    type: ACTIONS.DELETE_NOTE_SUCCESS,
    payload: {id},
  };
}


